The purpose of this NodeJS tool is to export a FBX file into a compressed GLTF ( Draco ) Format usable on the web. 
It's been developed on MacOS, there is no certainty this tool is compatible on Windows / Linux.

# Install
```
yarn install
```
# Usage

1 )  Place your FBX file in the input folder

2 ) ```node index.js yourfile.fbx```

3 ) If your model contains texture, you will be asked to select a powerof2 file size for each texture embedded in the model.

4 ) Your GLB ( draco binary format ) file is exported in output folder. ( Finder will open the output folder automatically at the end of the export )

# Options

There are tweakable constants inside index.js file : 


### Jpeg out quality
```
const JPEG_QUALITY = 60
```
### PNG out quality [ from, to ]
```
const PNG_QUALITY = [0.3, 0.5]
```
### Lit or Unlit material export => is using gltf khr-materials-unlit extension
```
const isUnLitMaterial = true
```
### Remove normal attributes of models if not needed => low file size
```
const removeNormals = true
```
### Activate Verbose mode
```
const verbose = true
```


# Tips

This tool was originally built to handle maya export.
During development we encoutered few problems we had to resolve concerning Maya export options.

### Frame reference
Place your sequencer cursor at the exact frame of the animated model you want to be as the frame reference ( static / no animation ) 

### Multi-track-animation
This tool support multi-track clip animation embedded in the FBX file.

https://knowledge.autodesk.com/support/maya/learn-explore/caas/CloudHelp/cloudhelp/2018/ENU/Maya-ManagingScenes/files/GUID-31148EE5-CAA8-48F2-9E51-3C9712EE8A14-htm.html

### Visibility key is not in GLTF spec yet
Use a small scale value instead

### Turn off Maya Segment scale compensate
https://knowledge.autodesk.com/support/maya/troubleshooting/caas/simplecontent/content/turning-segment-scale-compensate-maya-how-to-make-maya-rigs-play-nice-unity.html

