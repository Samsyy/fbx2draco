const select   = require('select-shell')



var stream = process.stdin;


function requestSize(){  

  return new Promise((resolve, reject)=>{

    var list = select(
      /* possible configs */
      {
        pointer: ' ▸ ',
        pointerColor: 'yellow',
        checked: ' ◉  ',
        unchecked:' ◎  ',
        checkedColor: 'blue',
        msgCancel: '',
        multiSelect: false,
        inverse: true,
        prepend: true
      }
    );

    list

        .option('512')
        .option('1024')
        .option('2048')
        .list();

    list.on('select', function(options){
      resolve(options[0].value)
      // process.exit(0);
    });

  })

}

exports.requestSize = requestSize