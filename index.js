// Jpeg out quality
const JPEG_QUALITY = 60
// PNG out quality
const PNG_QUALITY = [0.3, 0.5]
// Is material shaded or not
const isUnLitMaterial = true
// Remove normal attributes of models if not needed
const removeNormals = true
// Verbose mode
const verbose = true

// get command line argument
let args = []

process.argv.forEach(function(val, index, array) {
	args.push(val)
});

var name = args[2].split('.')
name = name[0]

console.log("parsing.. ")
console.log(name)

const shell = require('shelljs');
const fbx2gltf = require('fbx2gltf');
const select = require('./selector.js')
const fs = require("fs");
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminMozJpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imagemagick = require('imagemagick');
const terminalImage = require('terminal-image');
const glob = require('glob');
const {
	exec
} = require('child_process');

var folderInput = './inputs/'

var folderOutput = './outputs/'

var input = name + '.fbx'

var output = name

var pathOut = folderOutput + name + '/'

if (!fs.existsSync(pathOut)) {

	shell.mkdir('-p', pathOut);
}


function convertImages(inp) {

	return new Promise((resolve, reject) => {

		imagemin([inp + '/*.{jpg,png}'], {

			destination: inp,
			plugins: [
				imageminMozJpeg({
					quality: JPEG_QUALITY
				}),
				imageminPngquant({
					quality: PNG_QUALITY
				})
			]
		}).then((res) => {

			resolve()

		});

	})

}

async function resizeImage(inp) {

	let files = glob.sync(inp + '**/*.{png,jpg}');

	let sizes = []

	let g = 0
	while (g < files.length) {

		let ff = files[g].split('/')

		ff = ff[ff.length - 1]

		console.log(await terminalImage.file(files[g]))
		console.log("SIZE FOR FILE : ")
		console.log(ff)

		await select.requestSize().then((value) => {
			sizes.push(value)
		})

		g++
	}

	return new Promise((resolve, reject) => {

		var ps = []

		let i = 0
		while (i < files.length) {

			let maxSize = sizes[i]

			let inp = files[i]

			let p = new Promise((resolve, reject) => {

				imagemagick.identify(inp, function(err, features) {

					if (err) throw err;

					let w = features.width
					let h = features.height

					if (w > maxSize || h > maxSize) {

						let ratio = 1 / (w / maxSize)

						imagemagick.resize({
							srcPath: inp,
							dstPath: inp,
							width: w * ratio
						}, function(err, stdout, stderr) {
							if (err) throw err;
							console.log('resized ', inp);
							resolve()
						});

					} else {

						resolve()

					}

				});
			})

			ps.push(p)

			i++
		}

		Promise.all(ps).then(() => {

			resolve()
		})

	})

}

function convertGamma(inp) {

	return new Promise((resolve, reject) => {

		console.log(inp)

		let files = glob.sync(inp + '**/*.{png,jpg}');

		var ps = []

		let i = 0
		while (i < files.length) {

			let inp = files[i]

			let p = new Promise((resolve, reject) => {

				imagemagick.convert(

					[inp, '-gamma', '2.0', inp],

					function(err, stdout) {

						console.log(err, stdout)

						resolve()

					})
			})

			ps.push(p)

			i++
		}

		Promise.all(ps).then(() => {

			resolve()
		})

	})

}

function convertToGLTF() {

	let opts = ""

	if (verbose) {

		opts += " --verbose "
	}

	if (removeNormals) {

		opts += " -k position "
		opts += " -k uv0 "
		opts += " -k uv1 "
	}

	if (isUnLitMaterial) {

		opts += ' --khr-materials-unlit '
	}

	exec(
		"./node_modules/fbx2gltf/bin/Darwin/FBX2glTF " + opts + " --input " + folderInput + input + " --output " + pathOut + output, (error, stdout, stderr) => {

			if (error) {
				throw error;
			}
			console.log(stdout);

			console.log('success')

			let foldOutput = pathOut + name + '_out/'

			// convertGamma(foldOutput).then(()=>{

			resizeImage(foldOutput).then(() => {

				convertImages(foldOutput).then(() => {

					exec("gltf-pipeline -i " + foldOutput + output + ".gltf -o " + foldOutput + name + ".glb -d", (error, stdout, stderr) => {

						if (error) {
							throw error;
						}
						console.log(stdout);

						const path = process.cwd() + '/outputs/' + name + '/' + name + '_out/'

						console.log(path)

						exec('open ' + path);

					})



				})

			})
		}
	)

	// fbx2gltf(folderInput + input, pathOut + output, opts ).then(

	//   destPath => {

	// 	console.log('success')

	// 	let foldOutput = pathOut + name + '_out/'

	// 	// convertGamma(foldOutput).then(()=>{

	// 		resizeImage(foldOutput).then(()=>{

	// 			convertImages(foldOutput).then(()=>{

	// 				exec("gltf-pipeline -i "+foldOutput + output+" -o "+foldOutput + name+".glb -d")

	// 				const path = process.cwd() + '/outputs/' + name + '/' + name + '_out/'

	// 				exec('open ' + path);

	// 			})

	// 		})

	//   },
	//   (e)=>{

	//   	console.log(e)
	//   }
	// );

}

convertToGLTF()